using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
//    public Transform target;
//    public float range;
//    public string enemyTag = "Enemies";
    private Transform daEnemyTarget;
    public float targetRange;
    public string daEnemyTag = "Enemies";
    public float rotateSpeed;
    public Transform rotatingTurretPart;

    public float rateOfFire;
    public float timeToFireAgain;
    public GameObject NetPrefab;
    public Transform FireFromLocation;
    // Start is called before the first frame update
    void Start()
    {
//        InvokeRepeating("UpdateTarget", 0f, 0.5f);
       InvokeRepeating("UpdateDaTarget", 0f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (daEnemyTarget == null)
//        if (target == null);
        return;

        Vector3 dir = daEnemyTarget.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 daRotation = Quaternion.Lerp(rotatingTurretPart.rotation, lookRotation, Time.deltaTime * rotateSpeed).eulerAngles;
//        Vector3 daRotation = lookRotation.eulerAngles;
        rotatingTurretPart.rotation = Quaternion.Euler(0f, daRotation.y, 0f);

        if(timeToFireAgain <= 0f)
        {
            ShootNet();
            timeToFireAgain = 1 / rateOfFire;
        }
        timeToFireAgain -= Time.deltaTime;
    }

//    void UpdateTarget()
//    {
//        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
//        float shortestDistance = Mathf.Infinity;
//        GameObject nearestEnemy = null;
//        foreach (GameObject enemy in enemies)
//        {
//            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
//            if(distanceToEnemy < shortestDistance)
//            {
//                shortestDistance = distanceToEnemy;
//                nearestEnemy = enemy;
//            }
//        }
//        if (nearestEnemy != null && shortestDistance <= range)
//        {
//            target = nearestEnemy.transform;
//        }
//    }

    void UpdateDaTarget()
        {
            GameObject[] daEnemies = GameObject.FindGameObjectsWithTag("Enemies");
            float shortestDistanceToDaEnemy = Mathf.Infinity;
            GameObject nearestEnemy = null;

            foreach (GameObject daEnemy in daEnemies)
            {
                float distanceToDaEnemy = Vector3.Distance(transform.position, daEnemy.transform.position);
                if (distanceToDaEnemy < shortestDistanceToDaEnemy)
                {
                    shortestDistanceToDaEnemy = distanceToDaEnemy;
                    nearestEnemy = daEnemy;
                }
            }
        if (nearestEnemy != null && shortestDistanceToDaEnemy <= targetRange)
        {
            daEnemyTarget = nearestEnemy.transform;
        }
        else
        {
            daEnemyTarget = null;
        }
    }
    void ShootNet()
    {
//        Debug.Log("Shooting net at enemy");
        SoundManager.playSound();
        GameObject captureNetGo = (GameObject)Instantiate(NetPrefab, FireFromLocation.position, FireFromLocation.rotation);
        CaptureNet captureNet = captureNetGo.GetComponent<CaptureNet>();

        if(captureNet !=null)
        {
            captureNet.SeekTarget(daEnemyTarget);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
//        Gizmos.DrawWireSphere(transform.position, range);
        Gizmos.DrawWireSphere(transform.position, targetRange);
    }
}
