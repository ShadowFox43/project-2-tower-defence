using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureNet : MonoBehaviour
{
    private Transform daTarget;
    public float captureNetSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }


    public void SeekTarget(Transform _daTarget)
    {
        daTarget = _daTarget;
    }
    // Update is called once per frame
    void Update()
    {
        
        if (daTarget == null)
        {
            Destroy(gameObject);
            return;
        }


        Vector3 daDirection = daTarget.position - transform.position;
        float distanceThisFrame = captureNetSpeed * Time.deltaTime;
        if (daDirection.magnitude <= distanceThisFrame)
        {
            HitDaTarget();
            return;
        }

        transform.Translate(daDirection.normalized * distanceThisFrame, Space.World);
    }

    void HitDaTarget()
    {
        Debug.Log("We got a hit");
    }

    
}
