using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float daSpeed;
    private Transform daTarget;
    private int daWaypointIndex;
    // Start is called before the first frame update
    void Start()
    {
        daSpeed = 15f;
//        daSpeed = daSpeed = Random.Range(5.0f, 15.0f);
        daTarget = Waypoints.wayPoints[0];
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 daDirection = daTarget.position - transform.position;
        transform.Translate(daDirection.normalized * daSpeed * Time.deltaTime, Space.World);

        //switching target positions
        if (Vector3.Distance(transform.position, daTarget.position) <= 0.04f)
        {
            GetDaNextWaypoint();
        }
    }

    // Switching waypoints
    void GetDaNextWaypoint()
    {
        if (daWaypointIndex >= Waypoints.wayPoints.Length -1)
        {
            Destroy(gameObject);
            return;
        }
        daWaypointIndex++;
        daTarget = Waypoints.wayPoints[daWaypointIndex];
    }
}
