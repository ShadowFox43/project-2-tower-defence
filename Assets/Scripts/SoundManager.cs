using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip hitSound;
    static AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        hitSound = Resources.Load<AudioClip>("Net-Launched");
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void playSound()
    {
        audioSource.PlayOneShot(hitSound);
    }


}
